﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Merthsoft.WolframAlphaApi {
	public class Pod {
		[XmlAttribute("title")]
		public string Title { get; set; }

		[XmlAttribute("scanner")]
		public string Scanner { get; set; }

		[XmlAttribute("id")]
		public string Id { get; set; }

		[XmlAttribute("position")]
		public int Position { get; set; }

		[XmlAttribute("error")]
		public bool Error { get; set; }

		[XmlAttribute("numsubpods")]
		public int NumSubPods { get; set; }

		[XmlAttribute("primary")]
		public bool Primary { get; set; }

		[XmlElement("subpod")]
		public List<SubPod> SubPods { get; set; }

		[XmlElement("states", IsNullable=true)]
		public StateList States { get; set; }

		[XmlElement("infos", IsNullable = true)]
		public InfoList Infos { get; set; }

		public Pod() {
			SubPods = new List<SubPod>();
			States = new StateList();
			Infos = new InfoList();
		}
	}
}
