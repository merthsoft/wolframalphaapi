﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Merthsoft.WolframAlphaApi {
	public class InfoList {
		[XmlAttribute("count")]
		public int Count { get; set; }

		[XmlElement("info")]
		public List<Info> Infos { get; set; }

		public InfoList() {
			Infos = new List<Info>();
		}
	}
}
