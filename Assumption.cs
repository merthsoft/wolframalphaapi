﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace Merthsoft.WolframAlphaApi {
	public class Assumption {
		[XmlAttribute("type")]
		public string Type { get; set; }

		[XmlAttribute("word")]
		public string Word { get; set; }

		[XmlAttribute("template")]
		public string Template { get; set; }

		[XmlAttribute("count")]
		public int Count { get; set; }

		[XmlElement("value")]
		public List<AssumptionValue> Values { get; set; }

		public Assumption() {
			Values = new List<AssumptionValue>();
		}
	}
}
