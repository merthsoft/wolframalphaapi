﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Merthsoft.WolframAlphaApi {
	[XmlRoot("queryresult")]
	public class QueryResult {
		[XmlAttribute("success")]
		public bool Success { get; set; }

		[XmlAttribute("error")]
		public bool Error { get; set; }

		[XmlAttribute("numpods")]
		public int NumPods { get; set; }

		[XmlAttribute("version")]
		public decimal Version { get; set; }

		[XmlAttribute("datatypes")]
		public string DataTypes { get; set; }

		[XmlAttribute("timing")]
		public decimal TimeRequired { get; set; }

		[XmlAttribute("parsetiming")]
		public decimal ParseTimeRequired { get; set; }

		[XmlAttribute("parsetimesout")]
		public bool ParseTimedOut { get; set; }

		[XmlAttribute("Recalculate")]
		public string RecalculateUrl { get; set; }

		[XmlElement("pod")]
		public List<Pod> Pods { get; set; }

		[XmlElement("assumptions")]
		AssumptionList Assumptions { get; set; }

		public QueryResult() {
			Pods = new List<Pod>();
			Assumptions = new AssumptionList();
		}
	}
}
