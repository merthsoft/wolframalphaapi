﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Merthsoft.WolframAlphaApi {
	public class AssumptionValue {
		[XmlAttribute("name")]
		public string Name { get; set; }

		[XmlAttribute("desc")]
		public string Description { get; set; }

		[XmlAttribute("input")]
		public string Input { get; set; }
	}
}
