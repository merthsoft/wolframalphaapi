﻿Merthsoft.WolframAlphaApi
Merthsoft Creations, 2014
Shaun McFall

A simple wrapper around the Wolfram|Alpha API.

To use to include a reference to Merthsoft.WolframAlphaApi and call
WolframAlpha.Calculate("your math or whatever here"). Requires a reference
to Merthsoft.TinyUrlApi (https://bitbucket.org/merthsoft/tinyurlapi) to
generate a small URL when returning results.

Make sure you change the app id in the code to be your app id. Ideally
the function call would take that so it's not compiled in, but that wasn't
on my mind when I was making this.

This returns fairly basic information when calculating, basically just
the first result W|A gives. You can get deeper by enumerating the pods
and such, but for my purposes I just wanted it to be the basic answer.

This uses XML deserialization to pull get the data, but if that fails
it falls back to using a simple XPath type thing to find the data.