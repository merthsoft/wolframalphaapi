﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Merthsoft.WolframAlphaApi {
	public class Info {
		[XmlAttribute("url")]
		public string Url { get; set; }

		[XmlAttribute("text")]
		public string Text { get; set; }
	}
}
