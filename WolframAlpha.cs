﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using Merthsoft.TinyUrlApi;

namespace Merthsoft.WolframAlphaApi {
	public static class WolframAlpha {
		private const string APP_ID = "<your app id>";
		private const string API_URL = "http://api.wolframalpha.com/v2/query?appid=" + APP_ID + "&input={0}&format=plaintext";
		private const string STANDARD_URL = "http://www.wolframalpha.com/input/?i={0}";
		private const string TINY_URL_API = "http://tny.im/yourls-api.php?action=shorturl&url={0}&format=simple";
        private const string errorString = "Could not calculate. Try Wolfram|Alpha directly: {0}";


        public static string Calculate(string math) {
            math = Uri.EscapeDataString(math);

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(string.Format(API_URL, math));
            QueryResult result = null;
            XmlSerializer serializer = new XmlSerializer(typeof(QueryResult));
            using (Stream s = req.GetResponse().GetResponseStream()) {
                try {
                    result = (QueryResult)serializer.Deserialize(s);
                } catch {
                    return "Had to resort to old method. Please alert Merth: " + backupCalculate(math);
                }
            }

            string formattedErrorString = string.Format(errorString, getMainUrl(math));
            if (!result.Success) {
                return formattedErrorString;
            }

            StringBuilder ret;
            var pods = result.Pods.Where(p => p.Primary).ToList();

            if (pods.Count == 0) {
                pods = result.Pods.Where(p => p.Id != "Input").ToList();
            }

            if (pods.Count == 0) {
                return formattedErrorString;
            }

            if (pods.Count == 1) {
                ret = new StringBuilder(pods[0].SubPods[0].TextResult.Replace("\r\n", " / ").Replace("\n", " / "));
            } else {
                ret = new StringBuilder();
                foreach (Pod pod in pods) {
                    string title = pod.Title;
                    string data = pod.SubPods[0].TextResult.Replace("\r\n", " / ").Replace("\n", " / ");
                    if (string.IsNullOrWhiteSpace(data)) { continue; }
                    string formattedString = string.Format("{0}: {1} \\ ", title, data);
                    if (ret.Length + formattedString.Length > 200) {
                        if (ret.Length == 0) {
                            ret.Append(formattedString);
                        }
                        ret.AppendFormat("Rest of output too long: {0}  ", getMainUrl(math));
                        break;
                    }
                    ret.Append(formattedString);
                }

                ret.Length -= 2;
            }

            if (ret.Length == 0) {
                return formattedErrorString;
            } else {
                return ret.ToString();
            }
        }

        private static string getMainUrl(string math) {
			string mainUrl = string.Format(STANDARD_URL, math);
			return TinyUrl.GetIsGdUrl(mainUrl) ?? mainUrl;
		}

		public static string backupCalculate(string math) {
			math = Uri.EscapeDataString(math);
			HttpWebRequest req = (HttpWebRequest)WebRequest.Create(string.Format(API_URL, math));
			XmlDocument doc = new XmlDocument();
			using (Stream s = req.GetResponse().GetResponseStream()) {
				doc.Load(s);
			}

			XmlNode root = doc.SelectSingleNode("//queryresult");
			bool success;
			if (!bool.TryParse(root.Attributes["success"].Value, out success) || !success) {
				return "Could not calculate. Perhaps try ~calc.";
			}

			StringBuilder ret = new StringBuilder();
			XmlNodeList pods;
			pods = root.SelectNodes("//pod[contains(@primary,'true')]");
			if (pods.Count == 0) {
				pods = root.SelectNodes("//pod[@id!='Input']"); // and position() < 6]");
			}

			if (pods.Count == 0) { return "Could not calculate. Perhaps try ~calc."; }

			if (pods.Count == 1) { return pods[0].ChildNodes[0].InnerText.Replace("\r\n", " / ").Replace("\n", " / "); }

			foreach (XmlNode pod in pods) {
				string title = pod.Attributes["title"].Value;
				string data = pod.ChildNodes[0].InnerText.Replace("\r\n", " / ").Replace("\n", " / ");
				if (string.IsNullOrWhiteSpace(data)) { continue; }
				string formattedString = string.Format("{0}: {1} \\ ", title, data);
				if (ret.Length + formattedString.Length > 150) {
					string url;
					req = (HttpWebRequest)WebRequest.Create(string.Format(TINY_URL_API, string.Format(STANDARD_URL, math)));
					using (StreamReader s = new StreamReader(req.GetResponse().GetResponseStream())) {
						url = s.ReadToEnd();
					}
					ret.AppendFormat("Rest of output too long: {0} \\", url);
					break;
				}
				ret.AppendFormat(formattedString);
			}

			ret.Length -= 2;

			return ret.ToString();
		}
	}
}
