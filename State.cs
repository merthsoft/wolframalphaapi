﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Merthsoft.WolframAlphaApi {
	public class State {
		[XmlAttribute("name")]
		public string Name { get; set; }

		[XmlAttribute("input")]
		public string Input { get; set; }
	}
}
