﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Merthsoft.WolframAlphaApi {
	public class SubPod {
		[XmlAttribute("title")]
		public string Title { get; set; }

		[XmlElement("plaintext")]
		public string TextResult { get; set; }
	}
}
