﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Merthsoft.WolframAlphaApi {
	public class AssumptionList {
		[XmlAttribute("count")]
		public int Count { get; set; }

		[XmlElement("assumption")]
		public List<Assumption> Assumptions { get; set; }

		public AssumptionList() {
			Assumptions = new List<Assumption>();
		}
	}
}
