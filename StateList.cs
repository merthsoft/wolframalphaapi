﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Merthsoft.WolframAlphaApi {
	public class StateList {
		[XmlAttribute("count")]
		public int Count { get; set; }

		[XmlElement("state")]
		public List<State> States { get; set; }

		public StateList() {
			States = new List<State>();
		}
	}
}
